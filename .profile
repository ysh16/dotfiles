export GOPATH=$HOME/go
export PATH=$HOME/bin:$PATH:$GOPATH/bin
export EDITOR=nvim
export VISUAL=nvim
export PAGER='less -r'
export BROWSER=/usr/share/qutebrowser/scripts/open_url_in_instance.sh
