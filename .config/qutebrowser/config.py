# config.py
# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
config: ConfigAPI = config  # noqa: F821 pylint: disable=E0602,C0103
c: ConfigContainer = c  # noqa: F821 pylint: disable=E0602,C0103

config.load_autoconfig(True)

# Keybindings
config.bind(',v', 'spawn --userscript view_in_mpv')
config.bind(',V', 'hint links spawn --detach mpv --force-window=immediate {hint-url}')
config.bind(',c', 'spawn -u untrack-url -r {url}')
config.bind(',C', 'hint links spawn -u untrack-url -O -r {hint-url}')
config.bind('tNH', 'config-cycle -p -u *://*.{url:host}/* content.notifications.enabled true false ask')
config.bind('tNh', 'config-cycle -p -u *://{url:host}/* content.notifications.enabled true false ask')
config.bind('tNu', 'config-cycle -p -u {url} content.notifications.enabled true false ask')
config.unbind(';r')
config.bind(';rd', 'hint --rapid links download')
config.bind(';ry', 'hint --rapid links yank')

# Editor
c.editor.command = ["st", "-e", "vim", "+call cursor({line}, {column})", "{file}"]

# Spell check
c.spellcheck.languages = ["en-US"]

c.url.default_page = str(config.configdir / 'homepage.html')
c.url.start_pages = [c.url.default_page]

# Search engines
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}',
                       'br'     : 'https://search.brave.com/search?q={}',
                       'w'      : 'https://en.wikipedia.org/?search={}',
                       'ap'     : 'https://www.archlinux.org/packages/?q={}',
                       'au'     : 'https://aur.archlinux.org/packages/?K={}',
                       'aw'     : 'https://wiki.archlinux.org/index.php?search={}',
                       'gh'     : 'https://github.com/search?q={}',
                       'yt'     : 'https://www.youtube.com/results?search_query={}',
                       'mu'     : 'https://music.youtube.com/search?q={}',
                       'r'      : 'https://www.reddit.com/r/{unquoted}',
                       'tw'     : 'https://twitter.com/search?q={}',
                       'tu'     : 'https://twitter.com/{}',
                       'no'     : 'https://selca.kastden.org/noona/search/?an_op=cnt&an={}',
                       'vl'     : 'https://www.vlive.tv/search/all?query={}',
                       'nn'     : 'https://search.naver.com/search.naver?where=news&query={}',
                       'di'     : 'https://www.dict.cc/?s={}',
                       'tr'     : 'https://translate.google.com/#view=home&op=translate&sl=auto&tl=en&text={}',
                       'pa'     : 'https://papago.naver.com/?st={}',
                       'ur'     : 'https://www.urbandictionary.com/define.php?term={}'}
c.completion.open_categories = ["history", "searchengines", "quickmarks", "bookmarks"]

# Aliases
c.aliases['imgsearch'] = 'open -t https://yandex.com/images/search?rpt=imageview&url={url}'
c.aliases['paywall'] = 'open https://www.google.com/search?q=cache:{url}'
c.aliases['qr'] = 'spawn --userscript qr'

# Adblock
c.content.blocking.adblock.lists, [
    "https://easylist.to/easylist/easylist.txt",
    "https://easylist.to/easylist/easyprivacy.txt",
    "https://curben.gitlab.io/malware-filter/urlhaus-filter-online.txt",
    "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=1&mimetype=plaintext",
    ]

# Tabs are windows
c.tabs.tabs_are_windows = True
c.tabs.show = 'never'
c.tabs.favicons.show = 'never'
c.window.title_format = '{audio}{perc}{current_title}{title_sep}qutebrowser'
config.set('new_instance_open_target', 'window')
config.bind('u', ':undo -w')
config.bind('U', ':undo')

# Notifications
c.content.notifications.presenter = 'libnotify'

# Do not prompt for download location
c.downloads.location.prompt = False

# Smooth scrolling
c.scrolling.smooth = False

# Fake fullscreen
c.content.fullscreen.window = True

# Status bar
c.statusbar.widgets = ["keypress", "url", "scroll", "progress"]

# Fonts
c.fonts.default_size = '16px'
c.fonts.default_family = 'monospace'
c.fonts.web.family.fixed = 'monospace'

c.completion.height = '30%'

c.prompt.radius = 0

c.colors.webpage.preferred_color_scheme = 'dark'

# Colors
import subprocess

def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props

xresources = read_xresources('*')

black      =  xresources['*.color0']
red        =  xresources['*.color1']
green      =  xresources['*.color2']
yellow     =  xresources['*.color3']
blue       =  xresources['*.color4']
magenta    =  xresources['*.color5']
cyan       =  xresources['*.color6']
white      =  xresources['*.color7']
black_b    =  xresources['*.color8']
red_b      =  xresources['*.color9']
green_b    =  xresources['*.color10']
yellow_b   =  xresources['*.color11']
blue_b     =  xresources['*.color12']
magenta_b  =  xresources['*.color13']
cyan_b     =  xresources['*.color14']
white_b    =  xresources['*.color15']
bg         =  xresources['*.background']
fg         =  xresources['*.foreground']

c.colors.completion.category.bg = bg
c.colors.completion.category.border.bottom = bg
c.colors.completion.category.border.top = bg
c.colors.completion.category.fg = fg
c.colors.completion.even.bg = bg
c.colors.completion.odd.bg = bg
c.colors.completion.fg = fg
c.colors.completion.item.selected.bg = fg
c.colors.completion.item.selected.border.bottom = bg
c.colors.completion.item.selected.border.top = bg
c.colors.completion.item.selected.fg = bg
c.colors.completion.match.fg = red_b
c.colors.completion.scrollbar.bg = bg
c.colors.completion.scrollbar.fg = fg
c.colors.downloads.bar.bg = bg
c.colors.downloads.error.bg = bg
c.colors.downloads.error.fg = red_b
c.colors.downloads.start.bg = fg
c.colors.downloads.start.fg = bg
c.colors.downloads.stop.bg = bg
c.colors.downloads.stop.fg = fg
c.colors.downloads.system.bg = 'none'
c.colors.hints.bg = bg
c.colors.hints.fg = fg
c.hints.border = '0'
c.hints.radius = 0
c.colors.hints.match.fg = red_b
c.colors.keyhint.bg = bg
c.colors.keyhint.fg = magenta_b
c.colors.keyhint.suffix.fg = red_b
c.colors.messages.error.bg = bg
c.colors.messages.error.border = bg
c.colors.messages.error.fg = red_b
c.colors.messages.info.bg = bg
c.colors.messages.info.border = bg
c.colors.messages.info.fg = blue_b
c.colors.messages.warning.bg = bg
c.colors.messages.warning.border = bg
c.colors.messages.warning.fg = red_b
c.colors.prompts.bg = bg
c.colors.prompts.border = '0'
c.colors.prompts.fg = fg
c.colors.prompts.selected.bg = bg
c.colors.statusbar.caret.bg = fg
c.colors.statusbar.caret.fg = bg
c.colors.statusbar.caret.selection.bg = fg
c.colors.statusbar.caret.selection.fg = red_b
c.colors.statusbar.command.bg = fg
c.colors.statusbar.command.fg = bg
c.colors.statusbar.command.private.bg = fg
c.colors.statusbar.command.private.fg = bg
c.colors.statusbar.insert.bg = fg
c.colors.statusbar.insert.fg = bg
c.colors.statusbar.normal.bg = fg
c.colors.statusbar.normal.fg = bg
c.colors.statusbar.passthrough.bg = fg
c.colors.statusbar.passthrough.fg = red_b
c.colors.statusbar.private.bg = magenta
c.colors.statusbar.private.fg = bg
c.colors.statusbar.progress.bg = bg
c.colors.statusbar.url.error.fg = red_b
c.colors.statusbar.url.fg = bg
c.colors.statusbar.url.hover.fg = magenta_b
c.colors.statusbar.url.success.http.fg = bg
c.colors.statusbar.url.success.https.fg = bg
c.colors.statusbar.url.warn.fg = red_b
c.colors.tabs.bar.bg = bg
c.colors.tabs.even.bg = bg
c.colors.tabs.even.fg = fg
c.colors.tabs.indicator.error = red_b
c.colors.tabs.indicator.start = red_b
c.colors.tabs.indicator.stop = green_b
c.colors.tabs.indicator.system = 'none'
c.colors.tabs.odd.bg = bg
c.colors.tabs.odd.fg = fg
c.colors.tabs.selected.even.bg = bg
c.colors.tabs.selected.even.fg = fg
c.colors.tabs.selected.odd.bg = bg
c.colors.tabs.selected.odd.fg = fg
