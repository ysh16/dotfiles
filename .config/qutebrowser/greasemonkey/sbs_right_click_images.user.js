// ==UserScript==
// @name         sbs right click images
// @version      0.1
// @description  Enables right click on SBS visual boards
// @match        https://programs.sbs.co.kr/*/visualboard/*
// ==/UserScript==

document.body.addEventListener("contextmenu",
	function(e) { e.stopPropagation(); },
	true);
