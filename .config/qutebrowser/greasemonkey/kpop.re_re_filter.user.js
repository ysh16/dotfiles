// ==UserScript==
// @name         kpop.re regex filter
// @version      0.1.1
// @description  recursively filter posts by regex
// @match        *://kpop.re/*
// @grant        GM_addStyle
// ==/UserScript==

(function() {
	// DEFINE YOUR REGEX FILTER HERE
	const filter = none;

	let hidden = new Set();

	// set hidden post style
	GM_addStyle('.re-hidden { opacity: 0; }');
	GM_addStyle('.re-hidden:hover { opacity: 0.5; }');

	function hidePost(p) {
		p.querySelector('.post-message').classList.add('re-hidden');
		hidden.add(p.getAttribute('data-id'));
	}

	function handlePost(p) {
		let text = p.querySelector('.post-message').textContent;
		let smiles = '';
		p.querySelectorAll('.smile').forEach(function(s) {
				smiles += s.getAttribute('title');
		});
		if (filter.test(text + smiles)) {
			hidePost(p);
			return;
		}
		// if post's post links contain a hidden post then hide it too
		let links = p.querySelector('.post-message')
			.querySelectorAll('.post-link')
		for (let i = 0; i < links.length; i++) {
			if (hidden.has(links[i].getAttribute('data-id'))) {
				hidePost(p);
				return;
			}
		}
	}

	// handle initial posts
	window.addEventListener('site_fully_loaded', function() {
		let posts = document.querySelectorAll('.post');
		posts.forEach(function(p) { handlePost(p) });
	}, false);

	// listen for new posts
	window.addEventListener('new_post_hook', function(e) {
		handlePost(e.detail.post);
	}, false);
})();
