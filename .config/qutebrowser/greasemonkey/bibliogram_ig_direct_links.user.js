// ==UserScript==
// @version      0.1
// @description  redirect bibliogram files to instagram, helps with filenames and when the bibliogram is down
// @include      /https?://bibliogram.*/(image|video)proxy*/
// ==/UserScript==

(function() {
	'use strict';

	var url = window.location.href,
		regex = /https?:\/\/bibliogram\..*\/(image|video)proxy\/.+\.[a-z0-9]+\?url=/;

	// only redirect if not already
	if (regex.test(url)) {
		// get rid of image so it won't load twice
		var images = document.getElementsByTagName('img');
		if (images.length > 0) {
	  		images[0].parentNode.removeChild(images[0]);
		}
		var new_url = decodeURIComponent(url.replace(regex, ''));
		// make sure we don't loop infinitely
		if (new_url !== url) {
			// redirect
			window.location.replace(new_url);
		}
	}
}());
