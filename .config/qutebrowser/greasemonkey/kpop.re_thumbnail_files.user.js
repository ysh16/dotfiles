// ==UserScript==
// @name         kpop.re thumbnail files
// @version      0.5.2
// @description  display youtube embed thumbnails as if they were images
// 	thanks to:
// 		https://gitlab.com/loopvid/scripts/-/blob/master/userscripts/kpop.re_post_id_fix.user.js
// 		https://openuserjs.org/scripts/IUC/TokkiPlugin_MediaDownloadHelper
// 		https://kpop.re/wood/588838#590392
// @match        *://kpop.re/*
// ==/UserScript==

(function() {
	function getThumbnail(url) {
		let re = /^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)/i;
		let match = url.match(re)
		let id = (match) ? match[7] : null;
		return (id) ? 'https://i.ytimg.com/vi/' + id + '/mqdefault.jpg' : null;
	}

	function handlePost(p) {
		let files = p.querySelector('.post-files');
		if (!files) {
			files = document.createElement('div');
			files.className = 'post-files';
			p.querySelector('.post-body').insertAdjacentElement('afterbegin', files);
		}

		let embeds = p.querySelectorAll('.post-embed');
		embeds.forEach(function(e) {
			let link = e.getAttribute('href');
			let thumbnail = getThumbnail(link);
			let handled = false;
			p.querySelectorAll(".post-file-link").forEach(function(l) {
				if (l.attributes.href.value == link)
					handled = true;
			});
			if (!thumbnail || handled)
				return;

			let figure = document.createElement('figure');
			figure.className = 'post-file';
			let caption = document.createElement('figcaption');
			caption.className = 'post-file-info';
			if (link.length > 30)
				caption.innerText = link.substr(0, 30) + '...';
			else
				caption.innerText = link;
			figure.appendChild(caption);
			let a = document.createElement('a');
			a.className = 'post-file-link';
			a.href = link;
			a.target = '_blank';
			figure.appendChild(a);
			let ico = document.createElement('i');
			ico.className = 'fa fa-youtube-play post-file-badge post-file-video-badge';
			a.appendChild(ico);
			let img = document.createElement('img');
			img.className = 'post-file-thumb';
			img.src = thumbnail;
			img.style.maxWidth = '200px';
			img.style.maxHeight = '200px';
			img.loading = 'lazy';
			img.target = '_blank';
			a.appendChild(img);

			p.querySelector('.post-files').insertAdjacentElement('beforeend', figure);

			let fileCount = files.querySelectorAll('.post-file').length;
			if (fileCount >= 1)
				p.classList.add('post_file');
			if (fileCount > 1)
				p.classList.add('post_files');
		});
	}

	// handle initial posts
	window.addEventListener('site_fully_loaded', function() {
		let posts = document.querySelectorAll('.post');
		posts.forEach(function(p) { handlePost(p) });
	}, false);

	// listen for new posts
	window.addEventListener('new_post_hook', function(e) {
		handlePost(e.detail.post);
	}, false);
})();
