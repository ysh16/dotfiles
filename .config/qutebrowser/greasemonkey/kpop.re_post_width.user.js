// ==UserScript==
// @name         kpop.re post width
// @version      0.2.2
// @description  make posts wider so 6 images fit side by side
// @match        *://kpop.re/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.post { max-width: 1440px !important; }');
