-- stupid simple webm "maker" for mpv

function build_cmd()
	local input = mp.get_property("path")
	local title = mp.get_property("filename")
	local dotidx = title:reverse():find(".", 1, true)
	if dotidx then title = title:sub(1, -dotidx-1) end
	local output = title .. ".webm"
	local meta = "-metadata title=\"" .. title .. "\""

	local seek = mp.get_property("ab-loop-a")
	if seek == "no" then seek = "0" end
	local to = mp.get_property("ab-loop-b")
	if to == "no" then to = mp.get_property("duration") end

	local an = ""
	if mp.get_property("mute") == "yes" then an = "-an" end

	local vf = ""
	if mp.get_property("sub") ~= "no" and
	   mp.get_property("sub-visibility") == "yes" then 
		vf = '-vf "subtitles=\'file\\:' .. mp.get_property("path") .. '\':si=0"'
	end

	local opt = [[-map 0:v:0 -map 0:a:0 \
-threads 8 -row-mt 1 -tile-columns 6 -frame-parallel 0 \
-c:v libvpx-vp9 -b:v 0 -crf 35 \
-auto-alt-ref 1 -lag-in-frames 25 -g 128 -pix_fmt yuv420p \
-c:a libopus -b:a 128k]]

	return string.format(
		"ffmpeg -i %q \\\n-ss %s -to %s \\\n%s %s \\\n%s -pass 1 \\\n%s\n\n",
		input, seek, to, vf, "-an", opt, "-f webm -y /dev/null") ..
		string.format(
		"ffmpeg -i %q \\\n-ss %s -to %s \\\n%s %s \\\n%s -pass 2 \\\n%s \\\n%q",
		input, seek, to, vf, an, opt, meta, output)
end

function copy_cmd()
	local cmd = build_cmd();
	mp.osd_message(cmd)
	local pipe = io.popen("xclip -in -filter | xclip -in -selection clipboard", "w")
	pipe:write(cmd)
	pipe:close()
end

function exec_cmd()
	local cmd = build_cmd();
	mp.osd_message(cmd)
	os.execute(cmd)
end

mp.add_key_binding("Alt+w", "simple-webm-copy", copy_cmd)
mp.add_key_binding("Alt+W", "simple-webm-exec", exec_cmd)
