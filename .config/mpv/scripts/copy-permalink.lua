-- https://gist.github.com/olejorgenb/a5194d9bc183dbe0bfb02aac18fe37f9
-- Original author: Ole Jørgen Brønner (olejorgenb@yahoo.no)
-- Requirement: xclip
-- Installation: 
-- 'mkdir -p ~/.config/mpv/scripts && cp -i copy-permalink.lua ~/.config/mpv/scripts'

function copyPermalink()
  local uri = mp.get_property("path")
  -- %q might not be fully robust
  local bookmark = string.format("%q", uri)
  local pipe = io.popen("xclip -in -filter | sed 's/^\"//; s/\"$//' | xclip -in -selection clipboard", "w")
  pipe:write(bookmark)
  pipe:close()
  local msg = string.format("Yanked link to clipboard: %q", uri)
  mp.osd_message(msg)
end

-- mp.register_script_message("copy-permalink", copyPermalink)
mp.add_key_binding("y", "copy-permalink", copyPermalink)
