#!/usr/bin/python
# vlivelist.py - Takes URL to the desired channel's Star Board as argument.

import json
import requests
import sys

boardurl = sys.argv[1]
board = boardurl.split('/')[6]
url = 'https://www.vlive.tv/globalv-web/vam-web/post/v1.0/board-' + board \
        + '/videoPosts?appId=8c6cc7b45d2568fb668be6e05b6e5a3b' \
        + '&fields=originPost,officialVideo&sortType=LATEST&limit=100'
headers = {'referer': boardurl}
resp = requests.get(url, headers=headers)
while resp:
    data = json.loads(resp.text)
    for item in data['data']:
        if 'officialVideo' not in item:
            continue
        if 'LIVE_TO_VOD' not in item['officialVideo']['badges']:
            continue
        print('https://vlive.tv/video/', item['officialVideo']['videoSeq'],
              sep='', end='\t')
        print('#', item['officialVideo']['title'])
    if 'nextParams' not in data['paging']:
        break
    after = data['paging']['nextParams']['after']
    resp = requests.get(url + '&after=' + after, headers=headers)
