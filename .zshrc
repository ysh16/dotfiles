[[ $- != *i* ]] && return

setopt auto_cd
setopt longlistjobs
setopt nohup
setopt noglobdots

# vi mode
set -o vi
export KEYTIMEOUT=1
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey '^p' up-history
bindkey '^n' down-history
_zsh_cli_fg() { fg; }
zle -N _zsh_cli_fg
bindkey '^z' _zsh_cli_fg

# aliases
alias ls='ls -Fv --color=auto'
alias la='ls -a'
alias ll='ls -l'
alias lh='ls -hl'
alias mv='mv -i'
alias cp='cp -i'
alias grep='grep --color=auto'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias youtube-dl='yt-dlp --compat-options youtube-dl'

# functions
function mkcd take () {
  mkdir -p $@ && cd ${@:$#}
}

# prompt
autoload -Uz add-zsh-hook
autoload -U colors && colors
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:git*' formats "(%b%u%c) "
setopt prompt_subst
last_exit() {
	EXIT=$?
	[[ EXIT -ne 0 ]] \
		&& echo "$EXIT "
}
vcs_precmd() {
    vcs_info
}
add-zsh-hook precmd vcs_precmd
prompt='%{$fg[red]%}$(last_exit)%{$reset_color%}'
prompt+='%B%{$fg[blue]%}%~%{$reset_color%}%b '
prompt+='${vcs_info_msg_0_}'
prompt+='%# '

# terminal title
xterm_title_precmd () {
	print -Pn -- '\e]2;%~ %# - zsh\a'
}
xterm_title_preexec () {
	local cmd=${1[1,200]}
	print -Pn -- '\e]2;%~ %# '
	print -n -- "${(q)cmd}"
	print -n -- ' - zsh\a'
}
if [[ "$TERM" = (alacritty*|gnome*|tmux*|xterm*|st*) ]]; then
	add-zsh-hook precmd xterm_title_precmd
	add-zsh-hook preexec xterm_title_preexec
fi

# history
setopt append_history
setopt share_history
setopt extended_history
setopt histignorealldups
setopt histignorespace
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

# colors
eval $(dircolors -b)
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# completion
setopt completeinword
autoload -U compinit
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*:expand:*' tag-order all-expansions
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.*' insert-sections   true
zstyle ':completion:*:man:*' menu yes select
zstyle ':completion:*' special-dirs ..
zstyle ':completion:*:urls' local 'www' '/var/www/' 'public_html'
[[ -r ~/.ssh/config ]] && _ssh_config_hosts=(${${(s: :)${(ps:\t:)${${(@M)${(f)"$(<$HOME/.ssh/config)"}:#Host *}#Host }}}:#*[*?]*}) || _ssh_config_hosts=()
[[ -r ~/.ssh/known_hosts ]] && _ssh_hosts=(${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[\|]*}%%\ *}%%,*}) || _ssh_hosts=()
[[ -r /etc/hosts ]] && : ${(A)_etc_hosts:=${(s: :)${(ps:\t:)${${(f)~~"$(</etc/hosts)"}%%\#*}##[:blank:]#[^[:blank:]]#}}} || _etc_hosts=()
hosts=(
	$(hostname)
	"$_ssh_config_hosts[@]"
	"$_ssh_hosts[@]"
	"$_etc_hosts[@]"
	localhost
)
zstyle ':completion:*:hosts' hosts $hosts
zmodload zsh/complist
compinit
